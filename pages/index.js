import { useEffect, useMemo, useState } from "react";
import { authenticatedFetch } from "@shopify/app-bridge-utils";
import { useAppBridge } from "@shopify/app-bridge-react";


const Index = (props) => {
  const app = useAppBridge();
  const [data, setData] = useState({});

  const color = useMemo(() => {
    return data.color || '';
  }, [data]);
  const yes = useMemo(() => { return data.yes || ''; }, [data]);
  const no = useMemo(() => { return data.no || ''; }, [data]);

  const getUserPreferences = async () => {
    try {
      await authenticatedFetch(app)('/api/preferences', { method: 'get' }).then((data) => data.json()).then(response => {
        setData(response.data);
        return response
      });
    }
    catch (e) {
      console.error(e);
    }
  }

  const setUserPreferences = async () => {
    try {
      await authenticatedFetch(app)('/api/preferences', { body: JSON.stringify({ color, yes, no }), headers: { 'Content-type': 'application/json' }, method: 'post' }).then((data) => data.json()).then(response => {
        return response
      });
    }
    catch (e) {
      console.error(e);
    }
  }

  useEffect(() => {
    getUserPreferences();
  }, []);

  const onSubmitHandler = async (e) => {
    e.preventDefault();
    await setUserPreferences();
  }

  return (
    <form className="pt text-center" onSubmit={onSubmitHandler}>
      <div className="mb">
        <label className="d-inline-block w-150" htmlFor="color">color:</label>
        <input onChange={e => setData({ ...data, color: e.target.value })} value={color} id="color" placeholder="either hex or color name..." />
      </div>

      <div className="mb">
        <label className="d-inline-block w-150" htmlFor="yes">positive btn text:</label>
        <input onChange={e => setData({ ...data, yes: e.target.value })} value={yes} id="yes" />
      </div>

      <div className="mb">
        <label className="d-inline-block w-150" htmlFor="no">negative btn text:</label>
        <input onChange={e => setData({ ...data, no: e.target.value })} value={no} id="no" />
      </div>

      <button type="submit">submit</button>
    </form>
  )
};

export default Index;
